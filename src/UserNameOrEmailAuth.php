<?php

namespace Drupal\emaillogin;

use Drupal\user\UserAuthInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Password\PasswordInterface;

/**
 * Extends core User authentication credential validation to use email address.
 */
class UserNameOrEmailAuth implements UserAuthInterface {

  /**
   * Constructs a UserAuth object.
   *
   * @param \Drupal\user\UserAuthInterface $user_auth
   *   The UserAuth being decorated.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Password\PasswordInterface $password_checker
   *   The password service.
   */
  public function __construct(UserAuthInterface $user_auth, EntityManagerInterface $entity_manager, PasswordInterface $password_checker) {
    $this->userAuth = $user_auth;
    $this->entityManager = $entity_manager;
    $this->passwordChecker = $password_checker;
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate($username, $password) {
    $uid = FALSE;

    // Re-using standard username authentication, any changes there at least
    // will hence for username auth occour.
    // The email authentication is presently identical except for doing the
    // search by mail.
    if (!empty($username) && strlen($password) > 0
      && !($uid = $this->userAuth->authenticate($username, $password))
    ) {
      $account_search = $this->entityManager->getStorage('user')->loadByProperties(array('mail' => $username));

      if ($account = reset($account_search)) {
        if ($this->passwordChecker->check($password, $account->getPassword())) {
          // Successful authentication.
          $uid = $account->id();

          // Update user to new password scheme if needed.
          if ($this->passwordChecker->needsRehash($account->getPassword())) {
            $account->setPassword($password);
            $account->save();
          }
        }
      }
    }

    return $uid;
  }

}
