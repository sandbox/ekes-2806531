<?php

namespace Drupal\emaillogin\Tests;

use Drupal\user\Tests\UserLoginTest;
use Drupal\user\Entity\User;

/**
 * Ensure that login works as expected with mail value.
 *
 * @see \Drupal\user\Tests\UserLoginTest
 *
 * @group emaillogin
 */
class UserEmailLoginTest extends UserLoginTest {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'emaillogin',
  ];

  /**
   * Tests login with destination.
   */
  public function testLoginCacheTagsAndDestination() {
    $this->drupalGet('user/login');
    // The user login form says "Enter your <site name> username.", hence it
    // depends on config:system.site, and its cache tags should be present.
    $this->assertCacheTag('config:system.site');

    $user = $this->drupalCreateUser(array());
    $this->drupalGet('user/login', array('query' => array('destination' => 'foo')));
    $edit = array('name' => $user->getEmail(), 'pass' => $user->pass_raw);
    $this->drupalPostForm(NULL, $edit, t('Log in'));
    $this->assertUrl('foo', [], 'Redirected to the correct URL');
  }

  /**
   * Tests blocked user login attempt.
   */
  public function testBlockUserLogin() {
    $account = $this->drupalCreateUser([]);
    $account->block();
    $account->save();

    $this->drupalGet('user/login');
    $edit = array(
      'name' => $account->getEmail(),
      'pass' => $account->pass_raw,
    );
    $this->drupalPostForm(NULL, $edit, t('Log in'));

    $this->assertText(t('The user account has not been activated or is blocked.'));
  }

  /**
   * Make an unsuccessful login attempt.
   *
   * @param \Drupal\user\Entity\User $account
   *   A user object with name and pass_raw attributes for the login attempt.
   * @param mixed $flood_trigger
   *   (optional) Whether or not to expect that the flood control mechanism
   *    will be triggered. Defaults to NULL.
   *   - Set to 'user' to expect a 'too many failed logins error.
   *   - Set to any value to expect an error for too many failed logins per IP
   *   .
   *   - Set to NULL to expect a failed login.
   */
  public function assertFailedLogin($account, $flood_trigger = NULL) {
    $edit = array(
      'name' => $account->getEmail(),
      'pass' => $account->pass_raw,
    );
    $this->drupalPostForm('user/login', $edit, t('Log in'));
    $this->assertNoFieldByXPath("//input[@name='pass' and @value!='']", NULL, 'Password value attribute is blank.');
    $this->assertFieldByXPath("//input[@name='name']", $account->getEmail(), 'Login name field still contains the email address entered.');
    if (isset($flood_trigger)) {
      if ($flood_trigger == 'user') {
        $this->assertRaw(\Drupal::translation()->formatPlural($this->config('user.flood')->get('user_limit'), 'There has been more than one failed login attempt for this account. It is temporarily blocked. Try again later or <a href=":url">request a new password</a>.', 'There have been more than @count failed login attempts for this account. It is temporarily blocked. Try again later or <a href=":url">request a new password</a>.', array(':url' => \Drupal::url('user.pass'))));
      }
      else {
        // No uid, so the limit is IP-based.
        $this->assertRaw(t('Too many failed login attempts from your IP address. This IP address is temporarily blocked. Try again later or <a href=":url">request a new password</a>.', array(':url' => \Drupal::url('user.pass'))));
      }
    }
    else {
      $this->assertText(t('Unrecognized password, username or email address. Forgot your password?'));
    }
  }

}
